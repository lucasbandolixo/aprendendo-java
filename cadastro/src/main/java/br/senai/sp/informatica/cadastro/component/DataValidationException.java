package br.senai.sp.informatica.cadastro.component;

import org.springframework.validation.BindingResult;

import lombok.Getter;

public class DataValidationException extends DataException {
    @Getter
    private BindingResult result;

    public DataValidationException(String message, BindingResult result){
        super(message);
        this.result = result;
    }

    public DataValidationException(String message, Throwable t) {
        super(message, t);
    }
}